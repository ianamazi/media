$(document).ready(function(){
	$('#toggle-expand').click(function(){
		$('body').toggleClass('expanded');
	});
	$('.pdf').each(function(){ $('<a class="pdf-load loading" />').html('OPEN').insertAfter($(this)).click(function(){ 
		//alert($(this).prev().attr('href'));
		var vwr = $(this).next('iframe');
		if (vwr.length)  { vwr.toggle(); return; }
		$('<iframe />').addClass('pdf-viewer').attr('src', $(this).prev().attr('href')).insertAfter($(this));
		$(this).html('toggle viewer').removeClass('loading');
	}) });
});
