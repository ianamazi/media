<?php
$dlUrl = site_url('downloads/' . $relUrl, 1);

$fileName = false;


if (!is_dir($thisFol)) {
  $fileName = basename($relUrl);
  $relUrl = dirname($relUrl) . '/';

  $thisFol = site_file('downloads/' . $relUrl);
  $thisUrl = site_url($relUrl, 1);
  $dlUrl = site_url('downloads/' . $relUrl, 1);
}

$fol = scandir($thisFol); natsort($fol);
if (!$fileName) {
  $start = true;
  foreach ($fol as $f) {
    if($f == '.' || $f == '..' || is_file($thisFol . $f)) continue;
    if ($start) { echo 'FOLDERS<ul>'; $start = false; }
    echo sprintf('<li><a href="%s/">%s</a></li>' . PHP_EOL, $thisUrl . $f, str_replace('-', ' ', $f));
  }
  if (!$start) echo '</ul>' . PHP_EOL;
}

$start = true;
foreach ($fol as $f) {
  if($f == '.' || $f == '..' || is_dir($thisFol . $f) || $f == 'readme.txt') continue;
  $bits = explode('.', $f);
  if ($bits[1] == 'lrc' || $bits[1] == 'txt' || $bits[1] == 'jpg') continue;
  $f = $bits[0];
  if ($start) { echo 'FILES<ul>'; $start = false; }
  $extn = $bits[1] == 'zip' ? '.zip' : '/';
  $suffix = $extn !='/' ? ' ZIP' : '';
  $f_r = str_replace('_', ' ', str_replace('-', ' ', $f));
  echo sprintf('<li%s><a href="%s">%s</a></li>' . PHP_EOL, $fileName == $f ? ' class="playing"' : '', ($extn == '/' ? $thisUrl : $dlUrl) . $f . $extn, $f_r . $suffix);
}
if (!$start) echo '</ul>' . PHP_EOL;
?>
