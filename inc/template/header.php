<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title></title>
    <script src="<?php site_url('assets/jquery-3.3.1.js'); ?>"></script>
    <script src="<?php site_url('assets/bootstrap.js'); ?>"></script>
    <link rel="stylesheet" href="<?php site_url('assets/bootstrap.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php site_url('assets/tabber.css'); ?>" type="text/css" />
    <script src="<?php site_url('assets/tabber-minimized.js'); ?>"></script>
    <link rel="stylesheet" href="<?php site_url('assets/theme.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php site_url('assets/iviewer.css'); ?>" type="text/css" />
    <script src="<?php site_url('assets/iviewer.js'); ?>"></script>
    <link rel="icon" type="image/png" href="<?php site_url('assets/iviewer.png'); ?>" />
  </head>
  <body class="expanded">
<div id="wrapper" class="container">
  <div id="header" class="row">
    <div id="logo" class="col-md-3 col-sm-12">
      <a id="toggle-expand">&nbsp;</a>
      <a href="<?php site_url(''); ?>">
        <img src="<?php site_url('assets/iviewer.png');?>" alt="iviewer" height="32" width="32" />
        IViewer Web
      </a> [<a href="/" target="_blank" title="YieldMore.org">YM</a>]
    </div>
    <div id="welcome" class="col-md-9 col-sm-12">
      <?php if ($relUrl) echo '<a class="folder" href="../">UP</a> |' . PHP_EOL; ?>
      <?php if ($relUrl) echo sprintf('Viewing: <a href="%s">%s</a>' . PHP_EOL, $thisUrl, $relUrl); ?>
    </div>
  </div>
  <div id="wrap" class="row">
    <div id="menu" class="col-md-3 col-sm-12">
      <?php include "menu.php"; ?>
    </div>
    <div id="content" class="col-md-9 col-sm-12">
    
