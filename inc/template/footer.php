    </div>
  </div>
  <div id="footer" class="row">
      <div class="col-12">
        <span class="float-right">Powered by <a href="https://amadeus.panishq.com" target="_blank">Amadeus</a>.</span>
        Published @ <a href="/media/">YieldMore Media</a>.
        <span class="small">Please <b>respect the copyrights</b> of the owners.</span>
      </div>
  </div>
</div>

  </body>
</html>