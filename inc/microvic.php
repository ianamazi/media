<?php

function site_file($rel, $contents = false)
{
	global $fileRoot;
	if (!isset($fileRoot))
	{
		$fileRoot = $_SERVER['SCRIPT_FILENAME'];
		$pos = strripos($fileRoot, "/");
		$fileRoot = substr($fileRoot, 0, $pos + 1);
	}
	$rel = $fileRoot . $rel;
	return $contents ? file_get_contents($rel) : $rel;
}

function site_url($rel, $return = 0)
{
  if (!strncmp($rel, 'http://', strlen('http://'))) return $rel;
  global $urlRoot;
  if (!isset($urlRoot))
  {
    $urlRoot = $_SERVER['SCRIPT_NAME'];
    $urlRoot = str_ireplace("index.php", "", $urlRoot);
  }
  $rel = $urlRoot . $rel;
  if ($return) return $rel;
  echo $rel;
}
?>
