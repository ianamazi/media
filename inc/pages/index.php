<?php
$copy = false;
if (!$copy) $copy = file_exists($thisFol . '_copyright.txt') ? file_get_contents($thisFol . '_copyright.txt') : false;
if (!$copy) $copy = file_exists($thisFol . '/../_copyright.txt') ? file_get_contents($thisFol . '/../_copyright.txt') : false;
if ($copy)  echo '<blockquote class="copyright">' . $copy . '</blockquote>';

if (!$fileName) { 
	echo 'Click a folder/file on the left';
	if (file_exists($thisFol . 'readme.txt'))
		echo '<hr/>' . file_get_contents($thisFol . 'readme.txt');
	return;
}

//$fileName = urldecode($fileName); //NB: prefer replacing spaces with hyphens

$media = true;
if (file_exists($thisFol . $fileName . '.mp3'))
	$extension = 'mp3';
else if (file_exists($thisFol . $fileName . '.mp4'))
	$extension = 'mp4';
else if (file_exists($thisFol . $fileName . '.mov'))
	$extension = 'mov';
else if (file_exists($thisFol . $fileName . '.ogg'))
	$extension = 'ogg';
else
	$media = false;	

if ($media)
	mp3audio(array('url' => $dlUrl . $fileName . '.' . $extension, 'extension' => $extension), 0);

//docx / pdf / jpg
if (file_exists($thisFol . $fileName . '.docx'))
	echo sprintf('<a class="docx" target="_blank" rel="nofollow, noindex" href="%s">%s</a>', $dlUrl . $fileName . '.docx', $fileName . ' DOCX');

if (file_exists($thisFol . $fileName . '.pdf'))
	echo sprintf('<a class="pdf" target="_blank" rel="nofollow, noindex" href="%s">%s</a>', $dlUrl . $fileName . '.pdf', $fileName . ' PDF');

if (file_exists($thisFol . $fileName . '.jpg'))
  echo sprintf('<br/><img class="thumbnail" src="%s" />', $dlUrl . $fileName . '.jpg');

//html
$html = $thisFol . $fileName . '.html';
if (file_exists($html)) {
	echo file_get_contents($html);
	return;
}

//lrc / txt
$lrc = $thisFol . $fileName . '.lrc';

if (!file_exists($lrc))
  $lrc = $thisFol . $fileName . '.txt';

if (!file_exists($lrc)) return;

echo '<div class="verse">';
echo file_get_contents($lrc);
echo '</div>';

$quotes = $thisFol . $fileName . '-quotes.txt';
if (!file_exists($quotes)) return;

$quotes = explode('

', file_get_contents($quotes));

$quoteWanted = isset($_GET['quote']) ? $_GET['quote'] : '';
echo $quoteWanted;

echo '<div id="quotes" class="tabber">' . PHP_EOL;
foreach ($quotes as $q) {
	$bits = explode('
', $q, 2);
	if (substr($bits[0], 0, 2) == '##') {
		$lets = explode('/', substr($bits[0], 2)); //quotelets
		$relUrl = 'books/' . $lets[0] . '/' . $lets[1];
		$quoteUrl = site_url($relUrl . '?quote=' . $lets[2], 1);
		echo sprintf('<a href="%s">%s (%s) #%s</a><br />' . PHP_EOL, $quoteUrl, $lets[1], $lets[0], $lets[2]);
		echo sprintf('<div class="quotelet-comment">%s</div>' . PHP_EOL, $bits[1]);
		$letQuotes = explode('

', file_get_contents(site_file('downloads/' . $relUrl . '-quotes.txt')));
		foreach ($letQuotes as $lq) {
			$lqBits = explode('
', $lq, 2);
			if (substr($lqBits[0], 1) != $lets[2]) continue;
			echo $lqBits[1];
		}
		echo '<hr />';
		continue;
	}

	$id = substr($bits[0], 1);
	echo '  <div class="quote tabbertab' . ($quoteWanted == $id ? ' tabbertabdefault' : '') . '" id="quote-' . $id . '">' . PHP_EOL;
	echo '<h2>' . $id . '</h2>' . PHP_EOL;
	echo $bits[1];
	echo '  </div>';
}
echo '</div><div class="clear"></div>';
?>
