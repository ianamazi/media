Chapter 9: The King-Knowledge or The King-Secret

1. The Blessed Lord said: What I am going to tell thee, the uncarping, is the most secret thing of all, the essential knowledge attended with all the comprehensive knowledge, by knowing which thou shalt be released from evil.

2. This is the king-knowledge, the king-secret (the wisdom of all wisdoms, the secret of all secrets), it is a pure and supreme light which one can verify by direct spiritual experience, it is the right and just knowledge, the very law of being. It is easy to practise and imperishable.

3. (But faith is necessary). The soul that fails to get faith in the higher truth and law, O Parantapa, not attaining to Me, must return into the path of ordinary mortal living (subject to death and error and evil.)

4. By Me, all this universe has been extended in the ineffable mystery of My being; all existences are situated in Me, not I in them.

5. And yet all existences are not situated in Me, behold My divine Yoga; Myself is that which supports all being and constitutes their existence.

6. It is as the great, the all-pervading aerial principle dwells in the etheric that all existences dwell in Me, that is how you have to conceive of it.

7. All existences, O Kaunteya, return into My divine Nature (out of her action into her immobility and silence) in the lapse of the cycle; at the beginning of the cycle again I loose them forth.

8. Leaning - pressing down upon my own Nature (Prakriti) I create (loose forth into various being) all this multitude of existences, all helplessly subject to the control of Nature.

9. Nor do these works bind me, O Dhananjaya, for I am seated as if indifferent above, unattached to those actions.

10. I am the presiding control of my own action of Nature, (not a spirit born in her, but) the creative spirit who causes her to produce all that appears in the manifestation. Because of this, O Kaunteya, the world proceeds in cycles.

11. Deluded minds despise me lodged in the human body because they know not my supreme nature of being, Lord of all existences.

12. All their hope, action, knowledge are vain things (when judged by the Divine and eternal standard); they dwell in the Rakshasic and Asuric nature which deludes the will and the intelligence.

13. The great-souled, O Partha, who dwell in the divine nature know Me (the Godhead lodged in human body) as the Imperishable from whom all existences originate and so knowing they turn to Me with a sole and entire love.

14. Always adoring Me, steadfast in spiritual endeavour, bowing down to Me with devotion, they worship Me ever in Yoga.

15. Others also seek Me out by the sacrifice of knowledge and worship Me in My oneness and in every separate being and in all My million universal faces (fronting them in the world and its creatures).

16. I the ritual action, I the sacrifice, I the food-oblation, I the fire-giving herb, the mantra I, I also the butter, I the flame, the offering I.

17. I the Father of this world, the Mother, the Ordainer, the first Creator, the object of Knowledge, the sacred syllable OM and also the Rik, Sama and Yajur (Vedas).

18. I the path and goal, the upholder, the master, the witness, the house and country, the refuge, the benignant friend; I the birth and status and destruction of apparent existence, I the imperishable seed of all and their eternal resting-place.

19. I give heat, I withhold and send forth the rain; immortality and also death, existent and non-existent am I, O Arjuna.

20. The Knowers of the triple Veda, who drink the soma-wine, purify themselves from sin, worshipping Me with sacrifice, pray of Me the way to heaven: they ascending to the heavenly worlds by their righteousness enjoy in paradise the divine feasts of the gods.

21. They, having enjoyed heavenly worlds of larger felicities, the reward of their good deeds exhausted, return to mortal existence. Resorting to the virtues enjoined by the three Vedas, seeking the satisfaction of desire, they follow the cycle of birth and death.

22. To those men who worship Me making Me alone the whole object of their thought, to those constantly in Yoga with Me, I spontaneously bring every good.

23. Even those who sacrifice to other godheads with devotion and faith, they also sacrifice to Me, O son of Kunti, though not according to the true law.

24. It is I myself who am the enjoyer and the Lord of all sacrifices, but they do not know Me in the true principles and hence they fall.

25. They who worship the gods go to the gods, to the (divinised) Ancestors go the Ancestor-worshippers, to elemental spirits go those who sacrifice to elemental spirits; but My worshippers come to Me.

26. He who offers to Me with devotion a leaf, a flower, a fruit, a cup of water, that offering of love from the striving soul, is acceptable to Me.

27. Whatever thou doest, whatever thou enjoyest, whatever thou sacrificest, whatever thou givest, whatever energy of tapasya, of the soul's will or effort, thou puttest forth, make it an offering unto Me.

28. Thus shall thou be liberated from good and evil results which constitute the bonds of action; with thy soul in union with the Divine through renunciation, thou shall become free and attain to Me.

29. I (the Eternal Inhabitant) am equal in all existences, none is dear to Me, none hated; yet those who turn to Me with love and devotion, they are in Me and I also in them.

30. If even a man of very evil conduct turns to Me with a sole and entire love, he must be regarded as a saint, for the settled will of endeavour in him is a right and complete will.

31. Swiftly he becomes a soul of righteousness and obtains eternal peace. This is my word of promise, O Arjuna, that he who loves me shall not perish.

32. Those who takes refuge with Me, O Partha, though outcastes, born from a womb of sin, women, Vaishyas, even Shudras, they also attain to the highest goal.

33. How much rather then holy Brahmins and devoted king-sages; thou who hast come to this transient and unhappy world, love and turn to Me.

34. Become my minded, my lover and adorer, a sacrificer to me, bow thyself to me, thus united with me in the Self thou shalt come to me, having me as thy supreme goal.