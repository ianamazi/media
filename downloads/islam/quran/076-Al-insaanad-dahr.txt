76:1 - Hath there come upon man (ever) any period of time in which he was a thing unremembered?
76:2 - Lo! We create man from a drop of thickened fluid to test him; so We make him hearing, knowing.
76:3 - Lo! We have shown him the way, whether he be grateful or disbelieving.
76:4 - Lo! We have prepared for disbelievers manacles and carcans and a raging fire.
76:5 - Lo! the righteous shall drink of a cup whereof the mixture is of Kafur,
76:6 - A spring wherefrom the slaves of Allah drink, making it gush forth abundantly,
76:7 - (Because) they perform the vow and fear a day whereof the evil is wide-spreading,
76:8 - And feed with food the needy wretch, the orphan and the prisoner, for love of Him,
76:9 - (Saying): We feed you, for the sake of Allah only. We wish for no reward nor thanks from you;
76:10 - Lo! we fear from our Lord a day of frowning and of fate.
76:11 - Therefor Allah hath warded off from them the evil of that day, and hath made them find brightness and joy;
76:12 - And hath awarded them for all that they endured, a Garden and silk attire;
76:13 - Reclining therein upon couches, they will find there neither (heat of) a sun nor bitter cold.
76:14 - The shade thereof is close upon them and the clustered fruits thereof bow down.
76:15 - Goblets of silver are brought round for them, and beakers (as) of glass
76:16 - (Bright as) glass but (made) of silver, which they (themselves) have measured to the measure (of their deeds).
76:17 - There are they watered with a cup whereof the mixture is of Zanjabil,
76:18 - (The water of) a spring therein, named Salsabil.
76:19 - There wait on them immortal youths, whom, when thou seest, thou wouldst take for scattered pearls.
76:20 - When thou seest, thou wilt see there bliss and high estate.
76:21 - Their raiment will be fine green silk and gold embroidery. Bracelets of silver will they wear. Their Lord will slake their thirst with a pure drink.
76:22 - (And it will be said unto them): Lo! this is a reward for you. Your endeavour (upon earth) hath found acceptance.
76:23 - Lo! We, even We, have revealed unto thee the Qur&#39;an, a revelation;
76:24 - So submit patiently to thy Lord&#39;s command, and obey not of them any guilty one or disbeliever.
76:25 - Remember the name of thy Lord at morn and evening.
76:26 - And worship Him (a portion) of the night. And glorify Him through the livelong night.
76:27 - Lo! these love fleeting life, and put behind them (the remembrance of) a grievous day.
76:28 - We, even We, created them, and strengthened their frame. And when We will, We can replace them, bringing others like them in their stead.
76:29 - Lo! this is an Admonishment, that whosoever will may choose a way unto his Lord.
76:30 - Yet ye will not, unless Allah willeth. Lo! Allah is Knower, Wise.
76:31 - He maketh whom He will to enter His mercy, and for evil-doers hath prepared a painful doom.
