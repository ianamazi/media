44:1 - Ha. Mim.
44:2 - By the Scripture that maketh plain
44:3 - Lo! We revealed it on a blessed night - Lo! We are ever warning -
44:4 - Whereon every wise command is made clear
44:5 - As a command from Our presence - Lo! We are ever sending -
44:6 - A mercy from thy Lord. Lo! He, even He is the Hearer, the Knower,
44:7 - Lord of the heavens and the earth and all that is between them, if ye would be sure.
44:8 - There is no Allah save Him. He quickeneth and giveth death; your Lord and Lord of your forefathers.
44:9 - Nay, but they play in doubt.
44:10 - But watch thou (O Muhammad) for the day when the sky will produce visible smoke
44:11 - That will envelop the people. This will be a painful torment.
44:12 - (Then they will say): Our Lord relieve us of the torment. Lo! we are believers.
44:13 - How can there be remembrance for them, when a messenger making plain (the Truth) had already come unto them,
44:14 - And they had turned away from him and said: One taught (by others), a madman?
44:15 - Lo! We withdraw the torment a little. Lo! ye return (to disbelief).
44:16 - On the day when We shall seize them with the greater seizure, (then) in truth We shall punish.
44:17 - And verily We tried before them Pharaoh&#39;s folk, when there came unto them a noble messenger,
44:18 - Saying: Give up to me the slaves of Allah. Lo! I am a faithful messenger unto you.
44:19 - And saying: Be not proud against Allah. Lo! I bring you a clear warrant.
44:20 - And lo! I have sought refuge in my Lord and your Lord lest ye stone me to death.
44:21 - And if ye put no faith in me, then let me go.
44:22 - And he cried unto his Lord, (saying): These are guilty folk.
44:23 - Then (his Lord commanded): Take away My slaves by night. Lo! ye will be followed,
44:24 - And leave the sea behind at rest, for lo! they are a drowned host.
44:25 - How many were the gardens and the watersprings that they left behind,
44:26 - And the cornlands and the goodly sites
44:27 - And pleasant things wherein they took delight!
44:28 - Even so (it was), and We made it an inheritance for other folk;
44:29 - And the heaven and the earth wept not for them, nor were they reprieved.
44:30 - And We delivered the Children of Israel from the shameful doom;
44:31 - (We delivered them) from Pharaoh. Lo! he was a tyrant of the wanton ones.
44:32 - And We chose them, purposely, above (all) creatures.
44:33 - And We gave them portents wherein was a clear trial.
44:34 - Lo! these, forsooth, are saying:
44:35 - There is naught but our first death, and we shall not be raised again.
44:36 - Bring back our fathers, if ye speak the truth!
44:37 - Are they better, or the folk of Tubb&#39;a and those before them? We destroyed them, for surely they were guilty.
44:38 - And We created not the heavens and the earth, and all that is between them, in play.
44:39 - We created them not save with truth; but most of them know not.
44:40 - Assuredly the Day of Decision is the term for all of them,
44:41 - A day when friend can in naught avail friend, nor can they be helped,
44:42 - Save him on whom Allah hath mercy. Lo! He is the Mighty, the Merciful.
44:43 - Lo! the tree of Zaqqum,
44:44 - The food of the sinner!
44:45 - Like molten brass, it seetheth in their bellies
44:46 - As the seething of boiling water.
44:47 - (And it will be said): Take him and drag him to the midst of hell,
44:48 - Then pour upon his head the torment of boiling water.
44:49 - (Saying): Taste! Lo! thou wast forsooth the mighty, the noble!
44:50 - Lo! this is that whereof ye used to doubt.
44:51 - Lo! those who kept their duty will be in a place secured.
44:52 - Amid gardens and watersprings,
44:53 - Attired in silk and silk embroidery, facing one another.
44:54 - Even so (it will be). And We shall wed them unto fair ones with wide, lovely eyes.
44:55 - They call therein for every fruit in safety.
44:56 - They taste not death therein, save the first death. And He hath saved them from the doom of hell,
44:57 - A bounty from thy Lord. That is the supreme triumph.
44:58 - And We have made (this Scripture) easy in thy language only that they may heed.
44:59 - Wait then (O Muhammad). Lo! they (too) are waiting.
